#!/bin/sh
# Copyright (c) 2015 Boomi, Inc.

wget -nv $URL/atom/molecule_install64_no_jre.sh -P /tmp/
chmod a+x /tmp/molecule_install64_no_jre.sh
/tmp/molecule_install64_no_jre.sh -q -console -Vusername=${BOOMI_USERNAME} -Vpassword=${BOOMI_PASSWORD} \
  -VatomName=${BOOMI_ATOMNAME} -VaccountId=${BOOMI_ACCOUNTID} -VenvironmentId=${BOOMI_ENVIRONMENTID} \
  -VproxyHost=${PROXY_HOST} -VproxyUser=${PROXY_USERNAME} -VproxyPassword=${PROXY_PASSWORD} -VproxyPort=${PROXY_PORT} \
  -VlocalTempPath=${LOCAL_TEMP_PATH} -VlocalPath=${LOCAL_PATH} -Vsys.symlinkDir=${SYMLINKS_DIR} \
  -Vsys.installationDir=${INSTALLATION_DIRECTORY} -VinstallToken=${INSTALL_TOKEN} -Dinstall4j.debug=${INSTALL4J_DEBUG} \
  -Dinstall4j.logTimestamps=${INSTALL4J_DEBUG}  -Dinstall4j.logEncoding=UTF-8 \
  -Dinstall4j.detailStdout=${INSTALL4J_DEBUG} -Dinstall4j.logToStderr=${INSTALL4J_DEBUG} \
  ${VM_OPTIONS}

# Set some default molecule-specific container properties
container_properties_overrides="com.boomi.container.cloudlet.clusterConfig=UNICAST|"\
"com.boomi.container.localDir="${LOCAL_WORK_DIR}"|"\
"com.boomi.container.cloudlet.findInitialHostsTimeout=1|"\
"com.boomi.container.plugin.force.start.shared_http_server=true"
"${HOME}"/bin/override_properties "${container_properties_overrides}" "${ATOM_HOME}/conf/container.properties"
