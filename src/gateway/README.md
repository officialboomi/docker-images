# Overview

The Boomi Gateway Docker image includes:

* A minimal Alpine or Red Hat UBI base image
* The latest Boomi-supported version of Java
* Code to retrieve, set up, and invoke the Gateway 64-bit installer

**Note:** You must have a valid Boomi account to use this image. To sign up for a 30-day free trial account, visit [Boomi.com](https://boomi.com).


# Versioning

Boomi Docker images follow the semantic versioning represented by MAJOR.MINOR.PATCH:
* MAJOR version changes represent major image changes, such as changes to the base image, that may require changes to how the image is run in your environment.
* MINOR version changes represent enhancements and major defect fixes that should be backwards compatible.
* PATCH version changes represent security updates, embedded Java version updates, and minor defects.

The current supported version is tagged with "release". Images are not tagged with "latest" to force a more disciplined approach to versioning.

You can specify the OS and version to use:
* release -> Latest Alpine base (Alpine is our default OS)
* release-rhel -> Latest Red Hat UBI base
* 5.x.x -> Versioned Alpine base
* 5.x.x-alpine -> Versioned Alpine base
* 5.x.x-rhel -> Versioned Red Hat UBI base

**Note**: Customers wanting to connect to SAP systems using the Boomi SAP Connector should use our version 5 Red Hat image. The SAP JCo library used by the connector is incompatible with Alpine. Customers using version 3 should upgrade to version 5 to take advantage of a number of enhancements and security updates. See upgrade notes below.


## Version Support

Boomi supports the latest `release` and `release-rhel` tag for the current major version. If you experience any issues, be sure to update to the latest release to verify the issue before contacting support.

**Important**: Upon the availability of a new major version, the previous major version will be deprecated. When deprecated, the previous major version will continue to be supported and maintained (for example, quarterly Java updates, security patches, critical defects, but not minor defects nor enhancements) until its retirement date. After a minimum of six months, the previous major version will be retired after which it will no longer be supported or maintained.

Refer to the [Boomi feature deprecation process](https://help.boomi.com/docs/Atomsphere/Platform/atm-Boomi_feature_deprecation_process_policy_e4547744-664f-4539-bd23-b91676bcbd8a) for more information.


## Current Version

The current major version is 5.


## Changelog

See Runtime [Docker Image Changelog](https://help.boomi.com/docs/Atomsphere/Integration/Atom,%20Molecule,%20and%20Atom%20Cloud%20setup/c-atm-Docker_image_changelog).



# How to use this image

* ### Installing the initial node

Issue the following Docker run command with appropriate environment variables:
```
docker run -p 8077:8077 -h <nodeId> -e BOOMI_USERNAME=<user> -e BOOMI_PASSWORD=<pwd> -e BOOMI_ACCOUNTID=<accountId> -e BOOMI_ATOMNAME=<atomName> -e ATOM_LOCALHOSTID=<nodeId> --name <atomName><nodeId> -v <hostDirectory>:/mnt/boomi:Z -d boomi/gateway:<version>
```

If you do not already have a local copy of the image, this command pulls the image automatically.

After the command runs, the installer connects to the Boomi platform to authenticate your credentials. If authentication is successful, the runtime is created and the necessary files are installed. The runtime starts and runs as a daemon thread.

#### Installing additional nodes

You can install additional cluster nodes by updating the `<nodeId>` value for each new node:

```
docker run -h <nodeId> -e BOOMI_ATOMNAME=<atomName> -e ATOM_LOCALHOSTID=<nodeId> --name <atomName><nodeId> -v <hostDirectory>:/mnt/boomi:Z -d boomi/gateway:<version>
```

The BOOMI_ATOMNAME and `<hostDirectory>` values must match the values that you specified for the initial cluster node.

**Note:** If you want to work with a local copy of the Gateway Installer image, you can download the source code for the image from [Bitbucket](https://bitbucket.org/officialboomi/docker-images).

* ### Run the image via docker-compose:
Example `compose.yml` for cluster installation with multiple nodes.
```
x-gateway: &gateway
  image: boomi/gateway:<version>
  volumes:
    - <hostDirectory>:/mnt/boomi:Z
  environment: &env
    BOOMI_ATOMNAME: <atomName>
  restart: unless-stopped
services:
  node-0:
    <<: *gateway
    container_name: <atomName><node0_Id>
    hostname: <node0_Id>
    environment:
      <<: *env
      ATOM_LOCALHOSTID: <node0_Id>
      BOOMI_USERNAME: <user>
      BOOMI_PASSWORD: <pwd>
      BOOMI_ACCOUNTID: <accountId>
    ports: [8077:8077, 18077:18077]
    healthcheck:
      test: ["CMD", "wget", "-S", "http://localhost:8077/_admin/status", "-O", "/dev/null"]
      interval: 5s
      start_period: 120s
      retries: 5
  node-1:
    <<: *gateway
    container_name: <atomName><node1_Id>
    hostname: <node1_Id>
    environment:
      <<: *env
      ATOM_LOCALHOSTID: <node1_Id>
    depends_on:
      node-0:
        condition: service_healthy
  node-2:
    <<: *gateway
    container_name: <atomName><node2_Id>
    hostname: <node2_Id>
    environment:
      <<: *env
      ATOM_LOCALHOSTID: <node2_Id>
    depends_on:
      node-0:
        condition: service_healthy
``` 
Create a docker compose file and update the fields. Any common environment variables across all the nodes can be placed under yaml anchor `&env` and then run the command: `docker-compose -f compose.yml up`.

**Note:** If the first, or head, node installation takes long to finish and the other nodes can't start due to the failure of the health check, try increasing the number of retries or duration of the interval, or manually restart the Docker containers of the affected nodes.

# Environment variables

You can adjust the configuration of your local runtime by passing one or more environment variables on the `docker run ` command line.

* **BOOMI_USERNAME** - (Required in conjunction with BOOMI_PASSWORD; alternative to INSTALL_TOKEN) Specifies your Dell Boomi username.
* **BOOMI_PASSWORD** - (Required in conjunction with BOOMI_USERNAME; alternative to INSTALL_TOKEN) Specifies the password associated with the AtomSphere user.
* **INSTALL_TOKEN** - (Alternative to BOOMI_USERNAME and BOOMI_PASSWORD) Specifies a unique installer token. A token is valid only for the account in which it was generated. Tokens expire after a set amount of time ranging from 30 minutes to 24 hours.
* **BOOMI_ACCOUNTID** - (Required with BOOMI_USERNAME and BOOMI_PASSWORD; invalid with INSTALL_TOKEN) Specifies your Boomi account ID.
* **BOOMI_ATOMNAME** - (Required) Specifies the name of the Gateway that you are installing.
* **ATOM_LOCALHOSTID** - (Required) Specifies a unique and persistent local host ID, independent of any assigned IP address.
* **BOOMI_ENVIRONMENTID** - (Optional) Specifies the environment to which the Gateway is attached. If not explicitly specified, the Gateway appears in the Unattached Atoms list in Atom Management.
* **ATOM_VMOPTIONS_OVERRIDES** - (Optional) A | (pipe) separated list of vm options to set on a new installation.
* **CONTAINER_PROPERTIES_OVERRIDES** - (Optional) A | (pipe) separated list of container properties to set on a new installation.
* **LOCAL_PATH** - (Optional) Specifies the local file path (rather than the network file system). The local path must be consistent among all cluster nodes.
* **LOCAL_TEMP_PATH** - (Optional) Specifies the local temporary file path that is used by Java for temporary storage. The local temporary path must be consistent among all Gateway nodes.
* **PROXY_HOST** - (Optional) Specifies a proxy host name.
* **PROXY_USERNAME** - (Optional) Specifies the proxy's username.
* **PROXY_PASSWORD** - (Optional) Specifies the proxy user's password.
* **PROXY_PORT** - (Optional) Specifies the proxy's port number.
* **VALIDATE_MOUNT_ACCESS** - (Optional) Specifies whether permissions are validated for the mounted path. The default value is true.
* **UPDATE_MOUNT_PERMISSIONS** - (Optional) Specifies whether permissions of the mounted path are updated if needed.
  Note that this is not recursive. The default value is true. The environment variable VALIDATE_MOUNT_ACCESS must be
  set to true as well for this to take effect.


# Known Issues

* If, after using the docker run command, you receive an error about access to the mounted path, ensure that the
  mounted host directory has been given read, write, and execute permissions to UID 1000. Then, use the docker run
  command again with the environment variable `VALIDATE_MOUNT_ACCESS` set to false.

# Links

* [Official Boomi Docker Repository](https://bitbucket.org/officialboomi/docker-images/)
* [Official Boomi Runtime Containers Reference Architectures](https://bitbucket.org/officialboomi/runtime-containers/src/master/)