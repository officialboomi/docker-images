// Copyright (c) 2021 Boomi, Inc.
use anyhow::{Context, Result};
use std::env;

/// Returns the value of the given environment variable.
pub fn get_env_var(env_var: &str) -> Result<String> {
    let result =
        env::var(env_var).with_context(|| format!("Environment variable {} not found", env_var))?;
    Ok(result)
}

#[cfg(test)]
mod tests {
    use super::*;

    const ENV_VAR_TEST_KEY: &'static str = "TEST_KEY";
    const ENV_VAR_TEST_VALUE: &'static str = "TEST_VALUE";
    const ENV_VAR_DOES_NOT_EXIST: &'static str = "DOES_NOT_EXIST_KEY";

    #[test]
    fn successfully_get_env_var() {
        // given
        env::set_var(ENV_VAR_TEST_KEY, ENV_VAR_TEST_VALUE);

        // when
        let result = get_env_var(ENV_VAR_TEST_KEY);

        // then
        assert!(result.is_ok());
        assert_eq!(result.unwrap(), ENV_VAR_TEST_VALUE);
    }

    #[test]
    fn fail_get_env_var() {
        // given
        env::remove_var(ENV_VAR_DOES_NOT_EXIST);

        // when
        let result = get_env_var(ENV_VAR_DOES_NOT_EXIST);

        // then
        assert!(result.is_err());
        assert!(result.unwrap_err().to_string().contains("not found"));
    }
}
