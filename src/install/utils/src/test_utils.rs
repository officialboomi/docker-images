// Copyright (c) 2021 Boomi, Inc.
use anyhow::Result;
use rand::distributions::Alphanumeric;
use rand::Rng;
use std::env;
use std::fs;
use std::path::PathBuf;

pub fn random_alphanumeric() -> String {
    rand::thread_rng()
        .sample_iter(&Alphanumeric)
        .take(10)
        .map(char::from)
        .collect()
}

pub fn get_temp_directory() -> Result<PathBuf> {
    let file_name = random_alphanumeric();

    let dir = env::temp_dir().join(file_name);
    let _ = fs::remove_dir_all(&dir);
    fs::create_dir(&dir)?;

    Ok(dir)
}
