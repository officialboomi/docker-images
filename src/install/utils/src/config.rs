// Copyright (c) 2021 Boomi, Inc.
use nix::unistd::{Gid, Uid};
use std::path::PathBuf;

const INSTALLATION_DIRECTORY: &str = "/mnt/boomi";
const BOOMI_UID: u32 = 1000;
const BOOMI_GID: u32 = 1000;

#[derive(Default)]
pub struct Config;

#[cfg_attr(test, mockall::automock)]
pub trait ConfigService {
    /// Returns the installation directory of the A/M/C.
    fn installation_directory(&self) -> PathBuf;

    /// Returns the UID of the boomi user.
    fn uid(&self) -> Uid;

    /// Returns the GID of the boomi user.
    fn gid(&self) -> Gid;
}

impl ConfigService for Config {
    fn installation_directory(&self) -> PathBuf {
        PathBuf::from(INSTALLATION_DIRECTORY)
    }

    fn uid(&self) -> Uid {
        Uid::from_raw(BOOMI_UID)
    }

    fn gid(&self) -> Gid {
        Gid::from_raw(BOOMI_GID)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn get_installation_directory() {
        // when
        let result = Config::default().installation_directory();

        // then
        assert_eq!(result.to_str().unwrap(), INSTALLATION_DIRECTORY);
    }

    #[test]
    fn get_uid() {
        // when
        let result = Config::default().uid();

        // then
        assert_eq!(result.as_raw(), BOOMI_UID);
    }

    #[test]
    fn get_gid() {
        // when
        let result = Config::default().gid();

        // then
        assert_eq!(result.as_raw(), BOOMI_GID);
    }
}
