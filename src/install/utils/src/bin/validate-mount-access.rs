// Copyright (c) 2021 Boomi, Inc.
use anyhow::Result;
use runtime::config::Config;
use runtime::mount::{MountManager, MountManagerService};

fn main() -> Result<()> {
    let config = Box::new(Config::default());
    let mount_manager = MountManager::new(config);

    if !mount_manager.validate_access()? {
        std::process::exit(1);
    }

    Ok(())
}
