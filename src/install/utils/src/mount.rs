// Copyright (c) 2021 Boomi, Inc.
use crate::config::ConfigService;
use anyhow::Result;
use nix::unistd;
use nix::unistd::AccessFlags;
use std::fs;
use std::os::unix::fs::PermissionsExt;

const MOUNT_PERMISSIONS: u32 = 0o775;

pub struct MountManager {
    config: Box<dyn ConfigService>,
}

impl MountManager {
    pub fn new(config: Box<dyn ConfigService>) -> Self {
        MountManager { config }
    }
}

pub trait MountManagerService {
    /// Returns true if the user has read/write/access to the mounted installation directory.
    fn validate_access(&self) -> Result<bool>;

    /// Returns true if the owner, group, and permission bits have been
    /// successfully updated for the mounted installation directory.
    fn update_permissions(&self) -> Result<bool>;
}

impl MountManagerService for MountManager {
    fn validate_access(&self) -> Result<bool> {
        let install_dir = self.config.installation_directory();

        // Check for read permission
        if let Err(e) = unistd::access(&install_dir, AccessFlags::R_OK) {
            eprintln!(
                "Read permission not given for the installation directory - {}",
                e
            );
            return Ok(false);
        }

        // Check for write permission
        if let Err(e) = unistd::access(&install_dir, AccessFlags::W_OK) {
            eprintln!(
                "Write permission not given for the installation directory - {}",
                e
            );
            return Ok(false);
        }

        // Check for execute permission
        if let Err(e) = unistd::access(&install_dir, AccessFlags::X_OK) {
            eprintln!(
                "Execute permission not given for the installation directory - {}",
                e
            );
            return Ok(false);
        }

        Ok(true)
    }

    fn update_permissions(&self) -> Result<bool> {
        let install_dir = self.config.installation_directory();
        let uid = self.config.uid();
        let gid = self.config.gid();

        // Update the owner and group
        if let Err(e) = unistd::chown(&install_dir, Some(uid), Some(gid)) {
            eprintln!(
                "Unable to update the owner and group of the installation directory - {}",
                e
            );
            return Ok(false);
        }

        // Update the permissions
        if let Err(e) =
            fs::set_permissions(&install_dir, fs::Permissions::from_mode(MOUNT_PERMISSIONS))
        {
            eprintln!(
                "Unable to modify permissions of the installation directory - {}",
                e
            );
            return Ok(false);
        }

        Ok(true)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::config::MockConfigService;
    use crate::test_utils;
    use nix::unistd;

    #[test]
    fn no_read_access() -> Result<()> {
        // given
        let install_dir = test_utils::get_temp_directory()?;
        fs::set_permissions(&install_dir, fs::Permissions::from_mode(0o377))?;

        let mut config_service = MockConfigService::new();
        config_service
            .expect_installation_directory()
            .times(1)
            .returning(move || install_dir.clone());

        // when
        let result = MountManager::new(Box::new(config_service)).validate_access();

        // then
        assert!(!result.unwrap());

        Ok(())
    }

    #[test]
    fn no_write_access() -> Result<()> {
        // given
        let install_dir = test_utils::get_temp_directory()?;
        fs::set_permissions(&install_dir, fs::Permissions::from_mode(0o577))?;

        let mut config_service = MockConfigService::new();
        config_service
            .expect_installation_directory()
            .times(1)
            .returning(move || install_dir.clone());

        // when
        let result = MountManager::new(Box::new(config_service)).validate_access();

        // then
        assert!(!result.unwrap());

        Ok(())
    }

    #[test]
    fn no_execute_access() -> Result<()> {
        // given
        let install_dir = test_utils::get_temp_directory()?;
        fs::set_permissions(&install_dir, fs::Permissions::from_mode(0o677))?;

        let mut config_service = MockConfigService::new();
        config_service
            .expect_installation_directory()
            .times(1)
            .returning(move || install_dir.clone());

        // when
        let result = MountManager::new(Box::new(config_service)).validate_access();

        // then
        assert!(!result.unwrap());

        Ok(())
    }

    #[test]
    fn successful_access() -> Result<()> {
        // given
        let install_dir = test_utils::get_temp_directory()?;
        fs::set_permissions(&install_dir, fs::Permissions::from_mode(0o700))?;

        let mut config_service = MockConfigService::new();
        config_service
            .expect_installation_directory()
            .times(1)
            .returning(move || install_dir.clone());

        // when
        let result = MountManager::new(Box::new(config_service)).validate_access();

        // then
        assert!(result.unwrap());

        Ok(())
    }

    #[test]
    fn successfully_updated_permissions() -> Result<()> {
        // given
        let install_dir = test_utils::get_temp_directory()?;
        let install_dir_cloned = install_dir.clone();
        fs::set_permissions(&install_dir, fs::Permissions::from_mode(0o000))?;

        let mut config_service = MockConfigService::new();
        config_service
            .expect_installation_directory()
            .times(1)
            .returning(move || install_dir.clone());

        config_service
            .expect_uid()
            .times(1)
            .returning(|| unistd::Uid::current());

        config_service
            .expect_gid()
            .times(1)
            .returning(|| unistd::Gid::current());

        // when
        let result = MountManager::new(Box::new(config_service)).update_permissions();

        // then
        assert!(result.unwrap());
        assert!(unistd::access(&install_dir_cloned, AccessFlags::W_OK).is_ok());
        assert!(unistd::access(&install_dir_cloned, AccessFlags::R_OK).is_ok());
        assert!(unistd::access(&install_dir_cloned, AccessFlags::X_OK).is_ok());

        Ok(())
    }
}
