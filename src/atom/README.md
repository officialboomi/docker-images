# Overview

The Boomi Atom Docker image includes:

* A minimal Alpine or Red Hat UBI base image
* The latest Boomi-supported version of Java
* Code to retrieve, set up, and execute the Atom 64-bit installer

The Docker image does NOT include the Boomi Atom application itself, only the installer. The installer downloads and installs the Atom application as part of the container creation. Once installed, the Atom application is [self-updating](https://help.boomi.com/docs/Atomsphere/Platform/atm-Runtime_and_Atomsphere_releases_8aa6b48e-b0b6-4382-8ffa-a7cf23f0314f) from the Boomi Platform. You do not need to update the Docker image to obtain the latest Boomi Runtime release. The Docker image is updated when there are changes to how the container runs in your environment, or to apply security patches and updates to the Linux OS or Java.

**Note:** You must have a valid Boomi account to use this image. To sign up for a 30-day free trial account, visit [Boomi.com](https://boomi.com).

> [!WARNING]
> <b>Version 3 and 4 images will be retired and no longer support as of February 2025<b>
> <br/><br/>
>
> As of August 10, 2024 the previous major versions 3 and 4 are deprecated and will be retired in February 2025. Once retired the image versions will no longer be supported or maintained. Customers still using version 3 or version 4 images should upgrade to the latest version 5 image at their earlier convenience. For more information, see the [deprecation notice](https://community.boomi.com/s/article/Runtime-Docker-image-version-3-and-version-4-deprecation).


# Versioning

Boomi runtime Docker images follow the semantic versioning represented by MAJOR.MINOR.PATCH:

* MAJOR version changes represent major image changes, such as changes to the base image, that may require changes to how the image is run in your environment.
* MINOR version changes represent enhancements and major defect fixes that should be backwards compatible.
* PATCH version changes represent security updates, embedded Java version updates, and minor defects.

The current supported version is tagged with "release". Images are not tagged with "latest" to force a more disciplined approach to versioning.

You can specify the OS and version to use:
* release -> Latest Alpine base (Alpine is our default OS)
* release-rhel -> Latest Red Hat UBI base
* 5.x.x -> Versioned Alpine base
* 5.x.x-alpine -> Versioned Alpine base
* 5.x.x-rhel -> Versioned Red Hat UBI base

Boomi recommends using the `release` or `release-rhel` version when installing your containers to easily obtain the latest published image.

**Note**: Customers needing to connect to SAP systems using the SAP or SAP JCo V2 Connectors should use the version 5 Red Hat image. The SAP JCo library used by the connector is incompatible with Alpine. Customers using version 3 should upgrade to version 5 to take advantage of a number of enhancements and security updates. See upgrade notes below.


## Version Support

Boomi supports the latest `release` and `release-rhel` tag for the current major version. If you experience any issues, be sure to update to the latest release to verify the issue before contacting support.

**Important**: Upon the availability of a new major version, the previous major version will be deprecated. When deprecated, the previous major version will continue to be supported and maintained (for example, quarterly Java updates, security patches, critical defects, but not minor defects nor enhancements) until its retirement date. After a minimum of six months, the previous major version will be retired after which it will no longer be supported or maintained.

Refer to the [Boomi feature deprecation process](https://help.boomi.com/docs/Atomsphere/Platform/atm-Boomi_feature_deprecation_process_policy_e4547744-664f-4539-bd23-b91676bcbd8a) for more information.


## Current Version

The current major version is 5.

As of August 10, 2024 the previous major versions 3 and 4 are deprecated and will be retired in February 2025. Once retired the image versions will no longer be supported or maintained. Customers still using version 3 or version 4 images should upgrade to the latest version 5 image at their earlier convenience. For more information, see the [deprecation notice](https://community.boomi.com/s/article/Runtime-Docker-image-version-3-and-version-4-deprecation).


## Changelog

See Runtime [Docker Image Changelog](https://help.boomi.com/docs/Atomsphere/Integration/Atom,%20Molecule,%20and%20Atom%20Cloud%20setup/c-atm-Docker_image_changelog).


# Installing the Container

You can install the container with either Docker or Docker Compose.


## Installing with Docker

1. Generate a new install token via the [Boomi Platform UI](https://help.boomi.com/docs/Atomsphere/Integration/Atom,%20Molecule,%20and%20Atom%20Cloud%20setup/t-atm-Downloading_the_local_Atom_installer_401abba4-2ea0-43d7-9a15-4012f63467c1) or [Platform API](https://help.boomi.com/docs/Atomsphere/Integration/AtomSphere%20API/r-atm-Installer_Token_object_8bb87600-5048-4826-b716-f0173ca8798d).

2. Run the following Docker run command with appropriate environment variables. The minimum required variables are included here which are typically sufficient for most installs. See Environment Variable Reference below for the full list.

```
docker run -p 9090:9090 -h <atomName> -e INSTALL_TOKEN=<installerToken> -e BOOMI_ATOMNAME=<atomName> -e ATOM_LOCALHOSTID=<atomName> --name <atomName> -v <hostDirectory>:/mnt/boomi:Z 
 -d boomi/atom:<version>
```

Recommendations:

* `<atomName>` is the arbitrary user-defined name for your runtime. We recommend using the same value for the Boomi Atom name, Atom Local Host ID, Docker container name (`--name`), and Docker container hostname (`-h`)  for consistency.

* We recommend setting the version to `release` or `release-rhel` to use the latest available image version.

* We recommend using the INSTALL_TOKEN variable for authentication with the Boomi Platform but alternatively you can use the BOOMI_USERNAME, BOOMI_PASSWORD, and BOOMI_ACCOUNTID variables.

If you do not already have a local copy of the image, this command pulls the image automatically.

After the command runs, the installer connects to the Boomi platform to authenticate your credentials. If authentication is successful, the runtime is created and the necessary files are installed. The runtime starts and runs as a daemon thread.


## Installing with Docker Compose

1. Generate a new install token via the [Boomi Platform UI](https://help.boomi.com/docs/Atomsphere/Integration/Atom,%20Molecule,%20and%20Atom%20Cloud%20setup/t-atm-Downloading_the_local_Atom_installer_401abba4-2ea0-43d7-9a15-4012f63467c1) or [Platform API](https://help.boomi.com/docs/Atomsphere/Integration/AtomSphere%20API/r-atm-Installer_Token_object_8bb87600-5048-4826-b716-f0173ca8798d).

2. Create a Docker Compose file (see example below) and populate the variables for your account. See Environment Variable Reference below.

3. Run the following command:

```
docker-compose -f compose.yml up
```


### Exmaple compose.yml file

Example compose.yml for Atom installation.

```
services:
  atom:
    image: boomi/atom:<version>
    container_name: <atomName>
    volumes:
      - <hostDirectory>:/mnt/boomi:Z
    environment:
      - BOOMI_ATOMNAME=<atomName>
      - INSTALL_TOKEN=<installerToken>
      - ATOM_LOCALHOSTID=<atomName>
    ports: [9090:9090]
```

**Note**: We recommend using the INSTALL_TOKEN variable for authentication with the Boomi Platform but alternatively you can use the BOOMI_USERNAME, BOOMI_PASSWORD, and BOOMI_ACCOUNTID variables.


# Environment Variable Reference

You can adjust the configuration of your local runtime by passing one or more environment variables as part of the `docker run` command. Include additional environment variables in the Docker command line using the `-e` flag, or include in the environment section in the Docker Compose file.

* **INSTALL_TOKEN** - (Recommended) Specifies a unique installer token. A token is valid only for the account in which it was generated. Tokens expire after a set amount of time ranging from 30 minutes to 24 hours.
* **BOOMI_USERNAME** - (Alternative to INSTALL_TOKEN, required with BOOMI_PASSWORD) Specifies your Boomi username.
* **BOOMI_PASSWORD** - (Alternative to INSTALL_TOKEN, required with BOOMI_USERNAME) Specifies the password for your Boomi username.
* **BOOMI_ACCOUNTID** - (Alternative to INSTALL_TOKEN, required with BOOMI_USERNAME and BOOMI_PASSWORD) Specifies your Boomi account ID.
* **BOOMI_ATOMNAME** - (Required) Specifies the name of the Atom that you are installing.
* **ATOM_LOCALHOSTID** - (Required) Specifies a unique and persistent local host ID, independent of any assigned IP address.
* **BOOMI_ENVIRONMENTID** - (Optional) Specifies the environment to which the Atom is attached. If not explicitly specified, the Atom appears in the Unattached Atoms list in Atom Management.
* **ATOM_VMOPTIONS_OVERRIDES** - (Optional) A | (pipe) separated list of vm options to set on a new installation.
* **CONTAINER_PROPERTIES_OVERRIDES** - (Optional) A | (pipe) separated list of container properties to set on a new installation.
* **PROXY_HOST** - (Optional) Specifies a proxy host name.
* **PROXY_USERNAME** - (Optional) Specifies the proxy's username.
* **PROXY_PASSWORD** - (Optional) Specifies the proxy user's password.
* **PROXY_PORT** - (Optional) Specifies the proxy's port number.
* **VALIDATE_MOUNT_ACCESS** - (Optional) Specifies whether permissions are validated for the mounted path. The default value is true.
* **UPDATE_MOUNT_PERMISSIONS** - (Optional) Specifies whether permissions of the mounted path are updated if needed. Note that this is not recursive. The default value is true. The environment variable VALIDATE_MOUNT_ACCESS must be set to true as well for this to take effect.


# Known Issues

* If, after using the docker run command, you receive an error about access to the mounted path, ensure that the mounted host directory has been given read, write, and execute permissions to UID 1000. Then, use the docker run command again with the environment variable `VALIDATE_MOUNT_ACCESS` set to false.

* The SAP connector and the SAP JCo V2 connector are not compatible with the version 4 images. If you need to connect to your SAP systems, use the version 5 RHEL images instead.


# Incorporating the Boomi Image into a Custom Image

If you want to incorporate the Boomi installer into your own custom image, you can download the source code for the image from [Bitbucket⁠](https://bitbucket.org/officialboomi/docker-images). This may be desired if you want to use a different Linux OS distribution,  include additional environment variables for security purposes, or package Boomi alongside other applications for example. Note that you are responsible for maintaining and keeping your custom image up to date with the latest source code from Boomi.


# Upgrading Container Images

When a new image version is published, you need to upgrade your container to use that new version. Upgrading involves removing the previous container image and installing the new container image pointed to the same installation directory.


## Upgrading with Docker

1. Stop the running container.

```
docker stop <container name>
```

2. Remove the previous container.

```
docker rm <container name>
```

3. Install the container. **CRITICAL**: Be sure to use the same environment variable values as used for the original container installation (BOOMI_ATOMNAME, ATOM_LOCALHOSTID, name, and hostDirectory). The only difference will be the version, if necessary. We recommend setting the version to `release` or `release-rhel` to retrieve the latest available image.

```
docker run -p 9090:9090 -h <atomName> -e BOOMI_ATOMNAME=<atomName> -e ATOM_LOCALHOSTID=<atomName> --name <atomName> -v <hostDirectory>:/mnt/boomi:Z -d boomi/atom:<version>
```

4. The new container is now running with the latest image.


## Upgrading with Docker Compose

1. Stop and remove the container using the following command:

```
docker-compose down
```

2. Review the original compose.yml file to adjust the version, and other environment variables, if necessary (see upgrade considerations below). **CRITICAL**: Be sure to use the same environment variable values as used for the original container installation (BOOMI_ATOMNAME, ATOM_LOCALHOSTID, name, and hostDirectory).

3. Run the following command with the compose.yml file:

```
docker-compose -f compose.yml up
```

4. The new container is now running with the latest image.


## Upgrading from Version 4 to Version 5 Considerations

* There are no changes to the Docker install command or environment variables.

* Version 4 only offered an Alpine image. To continue to use Alpine, set the version to `release`. To switch to Red Hat, set the version to `release-rhel`.


## Upgrading from Version 3 to Version 5 Images

The version 5 image includes a number of changes to simplify the installation and configuration of the container image. One important consideration is whether to use the Alpine or Red Hat image. Review the upgrade notes below.

* If your integrations involve connecting to SAP applications using either the SAP connector or SAP JCo V2 connector, you must use the Red Hat version 5 image (version = `release-rhel`). The Alpine image does not support the SAP connectors.

* If the DOCKERUID environment variable was previously used and set to a value other than 1000 (not common), manual intervention is required prior to upgrading. Because the newer images are run with a non-root user with a hardcoded UID and GID of 1000, the mounted path where installed runtime files reside must be recursively updated to have an owner of UID 1000.

* Privileged mode is no longer necessary. The `--privileged` and `--tmpfs` flags are no longer used and should be removed from the Docker run command or Docker compose file. Also the mount to `/sys/fs/cgroup` should be removed.

* The following environment variables are no longer used and should be removed from the Docker run command or Docker compose file: BOOMI_CONTAINERNAME, DOCKERUID, HOST_INSTALLATION_DIRECTORY, INSTALLATION_DIRECTORY, SYMLINKS_DIR, and SECURITY_CRON.

* Custom images: If upgrading a custom image using the Boomi image source and looking to switch from Red Hat to Alpine OS, you will need to make appropriate changes to your custom image to account for the switch from Red Hat Enterprise Linux Universal Base Image (RHEL UBI) to Alpine.


# Links

* [Official Boomi Docker Repository](https://bitbucket.org/officialboomi/docker-images/)
* [Official Boomi Runtime Containers Reference Architectures](https://bitbucket.org/officialboomi/runtime-containers/src/master/)
