#!/bin/sh
# Copyright (c) 2015 Boomi, Inc.

wget -nv $URL/atom/atom_install64_no_jre.sh -P /tmp/
chmod a+x /tmp/atom_install64_no_jre.sh
/tmp/atom_install64_no_jre.sh -q -console -Vusername=${BOOMI_USERNAME} -Vpassword=${BOOMI_PASSWORD} \
  -VatomName=${BOOMI_ATOMNAME} -VaccountId=${BOOMI_ACCOUNTID} -VenvironmentId=${BOOMI_ENVIRONMENTID} \
  -VproxyHost=${PROXY_HOST} -VproxyUser=${PROXY_USERNAME} -VproxyPassword=${PROXY_PASSWORD} -VproxyPort=${PROXY_PORT} \
  -Vsys.symlinkDir=${SYMLINKS_DIR} -Vsys.installationDir=${INSTALLATION_DIRECTORY} -VinstallToken=${INSTALL_TOKEN} \
  -Dinstall4j.debug=${INSTALL4J_DEBUG} -Dinstall4j.logTimestamps=${INSTALL4J_DEBUG} \
  -Dinstall4j.logEncoding=UTF-8 -Dinstall4j.detailStdout=${INSTALL4J_DEBUG} -Dinstall4j.logToStderr=${INSTALL4J_DEBUG} \
  ${VM_OPTIONS}

  # Set some default atom-specific container properties
container_properties_overrides="com.boomi.container.plugin.force.start.shared_http_server=true"
"${HOME}"/bin/override_properties "${container_properties_overrides}" "${ATOM_HOME}/conf/container.properties"