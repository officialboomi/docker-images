# Boomi Docker Images
This repository contains source for the publicly available docker images on Docker Hub.

Refer to CHANGELOG.md for release information.

The version 3 images are in the [master branch](https://bitbucket.org/officialboomi/docker-images/src/master/).
## Atom
https://hub.docker.com/repository/docker/boomi/atom
## Molecule
https://hub.docker.com/repository/docker/boomi/molecule
## Cloud
https://hub.docker.com/repository/docker/boomi/cloud


# Building the Docker Images
From the root of dockerinst project.

# Building the Docker Images manually with Docker
#### Build redhat ubi minimal 9 install base
    docker build -t boomi/install-rhel:<version> ./src/install-rhel
#### Build install with rhel as base image
    docker build -t boomi/install:<version> --build-arg BASE_IMAGE=boomi/install-rhel:<version> ./src/install
#### Build alpine install base
    docker build -t boomi/install-alpine:<version> ./src/install-alpine
#### Build install with alpine as base image
    docker build -t boomi/install:<version> --build-arg BASE_IMAGE=boomi/install-alpine:<version> ./src/install
#### Build atom
    docker build -t boomi/atom:<version> --build-arg BASE_INSTALL_IMAGE=boomi/install:<version> ./src/atom
#### Build molecule
    docker build -t boomi/molecule:<version> --build-arg BASE_INSTALL_IMAGE=boomi/install:<version> ./src/molecule
#### Build cloud
    docker build -t boomi/cloud:<version> --build-arg BASE_INSTALL_IMAGE=boomi/install:<version> ./src/cloud
    
# Reference Architectures
https://bitbucket.org/officialboomi/runtime-containers/src/master/
