# Changelog

### release 5.1.2
- RUN-8012 Update Docker images to Java 11u25
- RUN-8011 Upgrade Redhat linux in docker image to 9.5-1731604394
- RUN-8008 Upgrade alpine images for docker to 3.20.3
- APIM-16400 Shared webserver property is set to false for docker gateway
- APIM-16447 Fix for Health check API results are stored in a file for docker gateway

### release 5.1.1
- RUN-7290 Update Docker images to Java 11u24
- RUN-7291 Upgrade Redhat linux in docker image to 9.4-1194
- RUN-7288 Upgrade alpine images for docker to 3.20.2

### release 5.1.0
- APIM-10108 Gateway Install (Docker Container)
- RUN-7120 Update Docker images to Java 11u23
- RUN-7123 Upgrade Redhat linux in docker image to 9.4-949.1716471857
- RUN-7119 Upgrade alpine images for docker to 3.20.0
- RUN-6976 Fix to solve the infinite number of readiness files creating on file share.
- RUN-7383 Upgrade rustc version to 1.67.0

### release 5.0.3
- RUN-6499 Update Docker images to Java 11u22
- RUN-6498 Upgrade Alpine linux in docker image
- RUN-6551 Upgrade Redhat linux in docker image

### release 5.0.2
- RUN-5987 Provide updated Docker image for Alpine (V5)

### release 5.0.1
- RUN-5871 Update Docker images to Java 11u21
- RUN-6267 Upgrade Alpine linux in docker image

### release 5.0.0
- RUN-5793 Provide updated Docker image for RedHat UBI 9 Minimal (V5)

### release 4.3.6
- RUN-5871 Update Docker images to Java 11u21
- RUN-6267 Upgrade Alpine linux in docker image

### release 4.3.5
- RUN-5884 Update Docker images to Java 11u20
- RUN-5889 Upgrade Alpine linux in docker image

### release 4.3.4
- RUN-5493 Upgrade Alpine linux in docker image
- RUN-5528 Update Docker images to Java 11u19

### release 4.3.3
- RUN-4848 Update Docker images to Java 11u18

### release 4.3.2
- RUN-3973 Update Rust from 1.56.1 to 1.63.0
- RUN-4551 Update Docker images to Java 11u17

### release 4.3.1
- RUN-4019 Update Docker images to Java 11u16
- RUN-4136 Update base image from Alpine 3.14.0 to 3.16.2

### release 4.3.0 
- RUN-1674 Add proxy configuration for basic authentication to docker images

### release 4.2.1
- RUN-3703 Update Docker images to Java 11u15

### release 4.2.0
- RUN-3429 The scaledown script does not wait for the pod to be offboarded in Kubernetes

### release 4.1.2
- RUN-3184 Update Docker images to Java 11u14

### release 4.1.1
- RUN-2932 Update Docker images to Java 11u13

### release 4.1.0
- RUN-3104 Error thrown when upgrading from an old installation to Docker image 4.0.0

### release 4.0.0
- RUN-1672 Images no longer require privileged mode and now run with a non-root user.

### release 3.2.8
- RUN-2612 Docker probes not working in version 3.2.7

### release 3.2.7
- RUN-2423 Update Docker images to Java 11u12
- RUN-1953 Pod does not start back up on AKS when liveliness probe fails: unknown signal "SIGRTMIN+3"

### release 3.2.6
- RUN-1993 Update Docker images to Java 11u11
- RUN-2080 Probes in Docker not working when using Java 11

### release 3.2.5
- RUN-1834 Update Docker images to Java 11u8

### release 3.2.4
- RUN-1684 Update Docker images to Java 1.8u281

### release 3.2.3
- RUN-1644 Docker installation (for 3.2.2) is unsuccessful because the permissions are not being properly set for the /usr/local/bin directory.

### release 3.2.2
- RUN-1418 Update Docker images to Java 1.8u271

### release 3.2.1
- RUN-1010 Permissions of files changing on certain systems during installation.

### release 3.2.0
- RUN-604 Added BOOMI_ENVIRONMENTID environment variable.
- RUN-604 Allow attaching a docker container to an environment during installation.
- RUN-969 Update to Java 8u261
- RUN-923 Docker installer should finish the installation successfully without setting optional environment variables.

### release 3.1.2
- RUN-958 Kubernetes probes failing in Docker Images 3.1.1

### release 3.1.1
- RUN-795 Update to Java 8u251
- RUN-683 Unable to set nonProxyHosts containing pipe | character in Docker container properties
- RUN-821 Remove support for docker installer script

### release 3.1.0
- Initial changelog tracking
- Kubernetes support version.
